﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab3
{
     public class DishListContext : DbContext
     {
          public DbSet<Dish> Dishes { get; set; }
          public DishListContext() 
          { 
               //Database.EnsureCreated();
          }
          protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
          {
               optionsBuilder.UseSqlServer(@"Server=localhost;Database=KPZ_Lab3_DB;Trusted_Connection=True;TrustServerCertificate=True");
          }
     }
}
