﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab3
{
     public class Dish : INotifyPropertyChanged
     {
          public Dish() { }
          public Dish(string name, decimal price, decimal weight, string type, int rating)
          {
               Name = name;
               Price = price;
               Weight = weight;
               Type = type;
               Rating = rating;
          }

          public event PropertyChangedEventHandler? PropertyChanged;
          public void OnPropertyChanged([CallerMemberName] string prop = "")
          {
               if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(prop));
          }

          private int id;
          private string name;
          private decimal price;
          private decimal weight;
          private string type;
          private int rating;
          public int Id
          {
               get { return id; }
               set
               {
                    id = value;
                    OnPropertyChanged("Id");
               }
          }
          public string Name
          {
               get { return name; }
               set
               {
                    name = value;
                    OnPropertyChanged("Name");
               }
          }
          public decimal Price
          {
               get { return price; }
               set
               {
                    price = value;
                    OnPropertyChanged("Price");
               }
          }
          public decimal Weight
          {
               get { return weight; }
               set
               {
                    weight = value;
                    OnPropertyChanged("Weight");
               }
          }
          public string Type
          {
               get { return type; }
               set
               {
                    type = value;
                    OnPropertyChanged("Type");
               }
          }
          public int Rating
          {
               get { return rating; }
               set
               {
                    rating = value;
                    OnPropertyChanged("Rating");
               }
          }
     }
}
