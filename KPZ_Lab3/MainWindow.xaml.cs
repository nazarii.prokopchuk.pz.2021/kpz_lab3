﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.IdentityModel.Tokens;

namespace KPZ_Lab3
{
   
     public partial class MainWindow : Window
     {
          private DishListContext db = new DishListContext();
          

          public MainWindow()
          {
               InitializeComponent();
               db.Dishes.Load();
               DishDataGrid.ItemsSource = db.Dishes.Local.ToObservableCollection();
          }

          private void AddButton_Click(object sender, RoutedEventArgs e)
          {
               string name = TextBox_Name.Text;
               decimal price = decimal.Parse(TextBox_Price.Text);
               decimal weight = decimal.Parse(TextBox_Weight.Text);
               string type = TextBox_Type.Text;
               int rating = int.Parse(TextBox_Rating.Text);
               Dish newDish = new Dish(name, price, weight, type, rating);

               db.Add(newDish);
               db.SaveChanges();
          }

          private void DeleteButton_Click(object sender, RoutedEventArgs e)
          {
               Dish dish = DishDataGrid.SelectedItem as Dish;

               if(dish != null)
               {
                    db.Remove(dish);
                    db.SaveChanges();
               }
          }

          private void UpdateButton_Click(object sender, RoutedEventArgs e)
          {
               Dish dish = DishDataGrid.SelectedItem as Dish;

               if (dish != null)
               {
                    if (!TextBox_Name.Text.IsNullOrEmpty())
                         dish.Name = TextBox_Name.Text;
                    if (!TextBox_Price.Text.IsNullOrEmpty())
                         dish.Price = decimal.Parse(TextBox_Price.Text);
                    if (!TextBox_Weight.Text.IsNullOrEmpty())
                         dish.Weight = decimal.Parse(TextBox_Weight.Text);
                    if (!TextBox_Type.Text.IsNullOrEmpty())
                         dish.Type = TextBox_Type.Text;
                    if (!TextBox_Rating.Text.IsNullOrEmpty())
                         dish.Rating = int.Parse(TextBox_Rating.Text);
                    db.Update(dish);
                    db.SaveChanges();
               }
          }
     }
}
